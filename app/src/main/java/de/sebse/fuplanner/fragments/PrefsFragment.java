package de.sebse.fuplanner.fragments;

import android.Manifest;
import android.accounts.Account;
import android.content.ContentResolver;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatDelegate;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.preference.ListPreference;
import androidx.preference.Preference;
import androidx.preference.PreferenceFragmentCompat;

import de.sebse.fuplanner.MainActivity;
import de.sebse.fuplanner.R;
import de.sebse.fuplanner.services.fulogin.AccountGeneral;
import de.sebse.fuplanner.services.kvv.sync.KVVContentProvider;
import de.sebse.fuplanner.tools.CustomAccountManager;
import de.sebse.fuplanner.tools.MainActivityListener;
import de.sebse.fuplanner.tools.Preferences;
import de.sebse.fuplanner.tools.RequestPermissionsResultListener;
import de.sebse.fuplanner.tools.logging.Logger;

public class PrefsFragment extends PreferenceFragmentCompat implements SharedPreferences.OnSharedPreferenceChangeListener {

    private MainActivityListener mMainActivityListener;
    private Logger log = new Logger(this);

    public static PrefsFragment newInstance() {
        PrefsFragment fragment = new PrefsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreatePreferences(Bundle savedInstanceState, String rootKey) {
        // Load the preferences from an XML resource
        setPreferencesFromResource(R.xml.preferences, rootKey);
        for (String s : getPreferenceScreen().getSharedPreferences().getAll().keySet()) {
            updateListPreferenceSummary(s);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        getPreferenceScreen().getSharedPreferences().registerOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        getPreferenceScreen().getSharedPreferences().unregisterOnSharedPreferenceChangeListener(this);
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String s) {
        updateListPreferenceSummary(s);

        if (s.equals(requireContext().getString(R.string.pref_sync_frequency))) {
            if (getActivity() != null && getActivity() instanceof MainActivity) {
                CustomAccountManager accountManager = ((MainActivity) getActivity()).getAccountManager();
                if (accountManager != null) {
                    Account accountByType = accountManager.getAccountByType(AccountGeneral.ACCOUNT_TYPE);
                    if (accountByType != null) {
                        ContentResolver.setSyncAutomatically(accountByType, KVVContentProvider.PROVIDER_NAME, true);
                        ContentResolver.addPeriodicSync(
                                accountByType,
                                KVVContentProvider.PROVIDER_NAME,
                                Bundle.EMPTY,
                                Long.parseLong(Preferences.getStringArray(getActivity(), R.array.pref_sync_frequency)));
                    }
                }
            }
        }

        if (s.equals(requireContext().getString(R.string.pref_night_mode))) {
            String nightMode = Preferences.getStringArray(requireContext(), R.array.pref_night_mode);
            switch (nightMode) {
                case "night":
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES);
                    break;
                case "day":
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO);
                    break;
                default:
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM);
                    break;
            }
        }

        if (s.equals(requireContext().getString(R.string.pref_add_calendar))
                && getActivity() != null
                && Preferences.getBoolean(getActivity(), R.string.pref_add_calendar)) {
            if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED
                    || ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.WRITE_CALENDAR, Manifest.permission.READ_CALENDAR}, 1);
            }
        }
        if (s.equals(requireContext().getString(R.string.pref_add_calendar)) && mMainActivityListener != null) {
            log.d("Force sync...");
            mMainActivityListener.forceSync();
        }
    }

    private void updateListPreferenceSummary(String s) {
        Preference preference = getPreferenceScreen().findPreference(s);
        if (preference instanceof ListPreference)
            preference.setSummary(((ListPreference) preference).getEntry());
    }


    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainActivityListener) {
            mMainActivityListener = (MainActivityListener) context;
            mMainActivityListener.onTitleTextChange(R.string.settings);
            mMainActivityListener.addRequestPermissionsResultListener(getRequestPermissionsResultListener(), "PrefFragment");
        } else
            throw new RuntimeException(context.toString() + " must implement MainActivityListener");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mMainActivityListener.removeRequestPermissionsResultListener("PrefFragment");
        mMainActivityListener = null;
    }

    private RequestPermissionsResultListener getRequestPermissionsResultListener() {
        return (requestCode, permissions, grantResults) -> {
            if (ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.WRITE_CALENDAR) != PackageManager.PERMISSION_GRANTED
            || ContextCompat.checkSelfPermission(requireContext(), Manifest.permission.READ_CALENDAR) != PackageManager.PERMISSION_GRANTED) {
                Preferences.setBoolean(requireContext(), R.string.pref_add_calendar, false);
                setPreferenceScreen(null);
                setPreferencesFromResource(R.xml.preferences, null);
            }
        };
    }
}