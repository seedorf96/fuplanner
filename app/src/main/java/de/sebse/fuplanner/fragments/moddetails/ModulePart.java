package de.sebse.fuplanner.fragments.moddetails;

public class ModulePart {
    static final int DESCRIPTION = 0;
    static final int OVERVIEW = 1;
    public static final int ANNOUNCEMENT = 2;
    public static final int ASSIGNMENT = 3;
    public static final int EVENT = 4;
    public static final int GRADEBOOK = 5;
    public static final int RESOURCES = 6;
    static final int LECTURERS = 7;
    private static final int[] pages = new int[]{OVERVIEW, ANNOUNCEMENT, ASSIGNMENT, GRADEBOOK, RESOURCES, EVENT};

    static int getPageCount() {
        return pages.length;
    }

    static int getPartByPage(int page) {
        return pages[page];
    }

    public static int getPageByPart(int part) {
        for (int i = 0; i < pages.length; i++) {
            int page = pages[i];
            if (page == part) {
                return i;
            }
        }
        return -1;
    }
}
