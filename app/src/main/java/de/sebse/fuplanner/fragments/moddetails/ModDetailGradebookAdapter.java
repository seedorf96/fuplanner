package de.sebse.fuplanner.fragments.moddetails;

import android.annotation.SuppressLint;
import android.util.Pair;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import de.sebse.fuplanner.R;
import de.sebse.fuplanner.services.kvv.types.Grade;
import de.sebse.fuplanner.services.kvv.types.Gradebook;
import de.sebse.fuplanner.services.kvv.types.Modules;
import de.sebse.fuplanner.tools.ui.StringViewHolder;

class ModDetailGradebookAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private static final int TYPE_TOTAL = 0;
    private static final int TYPE_GRADE = 1;

    private static final int SECTION_EXAM = 0;
    private static final int SECTION_ASSIGNMENT = 1;
    private static final int SECTION_OTHER = 2;

    private Modules.Module mValue;
    private Gradebook.CategorizedGrades mGrades;
    private final ArrayList<Pair<Integer, Integer>> mPositionalData;

    ModDetailGradebookAdapter() {
        mValue = null;
        mPositionalData = new ArrayList<>();
        //mListener = listener;
    }

    public void setModule(Modules.Module module) {
        mValue = module;
        this.setModule();
    }

    private void setModule() {
        mPositionalData.clear();
        if (mValue != null && mValue.gradebook != null) {
            mGrades = mValue.gradebook.getAutoCategorizedGrades();
            int size;
            size = mGrades.getExams().grades.size();
            if (size > 0) {
                mPositionalData.add(new Pair<>(setViewType(TYPE_TOTAL, SECTION_EXAM), 0));
                for (int i = 0; i < size; i++) {
                    mPositionalData.add(new Pair<>(setViewType(TYPE_GRADE, SECTION_EXAM), i));
                }
            }
            size = mGrades.getAssignments().grades.size();
            if (size > 0) {
                mPositionalData.add(new Pair<>(setViewType(TYPE_TOTAL, SECTION_ASSIGNMENT), 0));
                for (int i = 0; i < size; i++) {
                    mPositionalData.add(new Pair<>(setViewType(TYPE_GRADE, SECTION_ASSIGNMENT), i));
                }
            }
            size = mGrades.getOthers().grades.size();
            if (size > 0) {
                mPositionalData.add(new Pair<>(setViewType(TYPE_TOTAL, SECTION_OTHER), 0));
                for (int i = 0; i < size; i++) {
                    mPositionalData.add(new Pair<>(setViewType(TYPE_GRADE, SECTION_OTHER), i));
                }
            }
        } else {
            mGrades = null;
        }

        this.notifyDataSetChanged();
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view;
        switch (viewType) {
            case TYPE_TOTAL:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_moddetails_gradebook_title, parent, false);
                return new GradeTitleViewHolder(view);
            case TYPE_GRADE:
                view = LayoutInflater.from(parent.getContext())
                        .inflate(R.layout.list_moddetails_gradebook, parent, false);
                return new GradeViewHolder(view);
            default:
                //noinspection ConstantConditions
                return null;
        }
    }

    @Override
    public int getItemViewType(int position) {
        // Note that unlike in ListView adapters, types don't have to be contiguous
        if (position < mPositionalData.size())
            return getViewType(mPositionalData.get(position).first);
        else return -1;
    }

    @SuppressLint("StringFormatInvalid")
    @Override
    public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder holder, int position) {
        if (mValue == null || position > mPositionalData.size())
            return;
        Pair<Integer, Integer> data = mPositionalData.get(position);
        int viewType = getViewType(data.first);
        int viewSection = getViewSection(data.first);
        Gradebook.GradebookOutput gradebook = getViewSectionGradebook(viewSection);
        switch (viewType) {
            case TYPE_TOTAL:
                GradeTitleViewHolder h = (GradeTitleViewHolder) holder;

                if (gradebook != null) {
                    String title = "";
                    switch (viewSection) {
                        case SECTION_EXAM: title = h.mView.getResources().getString(R.string.exam); break;
                        case SECTION_ASSIGNMENT: title = h.mView.getResources().getString(R.string.assignments); break;
                        case SECTION_OTHER: title = h.mView.getResources().getString(R.string.others); break;
                    }
                    h.mTitle.setText(title);
                    Grade bestGrade = gradebook.getBestGrade();
                    h.mString.setText(h.mView.getResources().getString(
                            R.string.current_percentage,
                            gradebook.getUserPointSum(),
                            gradebook.getMaxPointSum(),
                            gradebook.getPercentage() * 100,
                            bestGrade == null ? 0 : bestGrade.getPoints(),
                            bestGrade == null ? 0 : bestGrade.getMaxPoints(),
                            bestGrade == null ? 0 : bestGrade.getPoints() / bestGrade.getMaxPoints() * 100,
                            bestGrade == null ? "" : bestGrade.getItemName()
                    ));
                }
                break;
            case TYPE_GRADE:
                int index = data.second;
                GradeViewHolder i = (GradeViewHolder) holder;

                if (gradebook != null) {
                    Grade grade = gradebook.grades.get(index);

                    i.mTitle.setText(grade.getItemName());
                    i.mGrade.setText(String.valueOf(grade.getPoints()));
                    i.mGradeMax.setText(String.valueOf(grade.getMaxPoints()));
                }
                break;
        }
    }

    @Override
    public int getItemCount() {
        return mPositionalData.size();
    }

    private int setViewType(int type, int section) {
        return type * 1024 + section;
    }

    private int getViewType(int combinedViewType) {
        return combinedViewType / 1024;
    }

    private int getViewSection(int combinedViewType) {
        return combinedViewType - getViewType(combinedViewType) * 1024;
    }

    private Gradebook.GradebookOutput getViewSectionGradebook(int viewSection) {
        if (mGrades != null) {
            switch (viewSection) {
                case SECTION_EXAM: return mGrades.getExams();
                case SECTION_ASSIGNMENT: return mGrades.getAssignments();
                case SECTION_OTHER: return mGrades.getOthers();
            }
        }
        return null;
    }









    private class GradeViewHolder extends RecyclerView.ViewHolder {
        private final TextView mGrade;
        private final TextView mGradeMax;
        private final TextView mTitle;

        GradeViewHolder(View view) {
            super(view);
            mTitle = view.findViewById(R.id.title);
            mGrade = view.findViewById(R.id.grade);
            mGradeMax = view.findViewById(R.id.grade_max);
        }

        @NonNull
        @Override
        public String toString() {
            return super.toString() + " '" + mTitle.getText() + " '" + mGrade.getText() + " '" + mGradeMax.getText() + "'";
        }
    }

    private class GradeTitleViewHolder extends StringViewHolder {
        private final TextView mTitle;

        GradeTitleViewHolder(View view) {
            super(view);
            mTitle = view.findViewById(R.id.title);
        }

        @NonNull
        @Override
        public String toString() {
            return super.toString() + " '" + mTitle.getText();
        }
    }
}
