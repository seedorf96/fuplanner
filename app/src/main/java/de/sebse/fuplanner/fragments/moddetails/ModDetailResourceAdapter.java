package de.sebse.fuplanner.fragments.moddetails;

import java.util.ArrayList;
import java.util.List;

import de.sebse.fuplanner.services.kvv.types.Modules;
import de.sebse.fuplanner.services.kvv.types.Resource;
import de.sebse.fuplanner.tools.ui.treeview.TreeNode;
import de.sebse.fuplanner.tools.ui.treeview.TreeViewAdapter;
import de.sebse.fuplanner.tools.ui.treeview.TreeViewBinder;

class ModDetailResourceAdapter extends TreeViewAdapter {
    private Modules.Module mValue;
    public ModDetailResourceAdapter(List<? extends TreeViewBinder> viewBinders) {
        super(viewBinders);
        mValue = null;
    }

    public void setModule(Modules.Module module) {
        mValue = module;
        this.setModule();
    }

    private void setModule() {
        if (mValue == null || mValue.resources == null) {
            return;
        }
        List<TreeNode> nodes = new ArrayList<>();
        for (Resource res: mValue.resources) {
            nodes.add(res.getTreeNode());
        }
        refresh(nodes);
    }
}
