package de.sebse.fuplanner.fragments.canteen;


import android.content.Context;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import de.sebse.fuplanner.MainActivity;
import de.sebse.fuplanner.R;
import de.sebse.fuplanner.services.canteen.CanteenBrowser;
import de.sebse.fuplanner.tools.MainActivityListener;
import de.sebse.fuplanner.tools.logging.Logger;

/**
 * A simple {@link Fragment} subclass.
 */
public class CanteensAddFragment extends Fragment implements CanteensAddAdapter.CanteenAddedInterface {
    private CanteensAddAdapter adapter;
    private SwipeRefreshLayout swipeLayout;
    private MainActivityListener mMainActivityListener;
    private final Logger log = new Logger(this);


    public CanteensAddFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     *
     * @return A new instance of fragment CanteensAddFragment.
     */
    // TODO: Rename and change types and number of parameters
    public static CanteensAddFragment newInstance() {
        CanteensAddFragment fragment = new CanteensAddFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_canteens_add, container, false);

        // Set the adapter
        Context context = view.getContext();
        RecyclerView recyclerView = view.findViewById(R.id.list);
        recyclerView.setLayoutManager(new LinearLayoutManager(context));
        adapter = new CanteensAddAdapter(context, this);
        recyclerView.setAdapter(adapter);

        // Getting SwipeContainerLayout
        swipeLayout = view.findViewById(R.id.swipe_container);
        // Adding Listener
        swipeLayout.setOnRefreshListener(() -> refresh(true));
        refresh(false);
        EditText searchBox = view.findViewById(R.id.search_box);
        searchBox.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                adapter.setFilterString(charSequence.toString());
            }

            @Override
            public void afterTextChanged(Editable editable) {

            }
        });

        return view;
    }

    @Override
    public void onAttach(@NonNull Context context) {
        super.onAttach(context);
        if (context instanceof MainActivityListener) {
            mMainActivityListener = (MainActivityListener) context;
            mMainActivityListener.onTitleTextChange(R.string.canteens);
        } else
            throw new RuntimeException(context.toString() + " must implement MainActivityListener");
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mMainActivityListener = null;
    }

    private void refresh(boolean forceRefresh) {
        if (getActivity() != null) {
            CanteenBrowser browser = ((MainActivity) getActivity()).getCanteenBrowser();
            browser.getAvailableCanteens(success -> {
                adapter.setCanteens(success);
                swipeLayout.setRefreshing(false);
            }, error -> {
                log.e(error.toString());
                swipeLayout.setRefreshing(false);
            }, forceRefresh);
        }
    }

    @Override
    public void onCanteenAdded(int canteenId) {
        if (getActivity() != null) {
            CanteenBrowser browser = ((MainActivity) getActivity()).getCanteenBrowser();
            browser.getCanteens(success -> {}, error -> {}, true);
        }
    }
}
