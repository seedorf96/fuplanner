package de.sebse.fuplanner.tools.ui;

import android.view.View;
import android.widget.TextView;

import com.cunoraz.tagview.TagView;

import androidx.annotation.NonNull;
import de.sebse.fuplanner.R;

public class AnnouncementViewHolder extends ExpandableCardViewHolder {
    public final TextView mTitle;
    public final TextView mSubTitle;
    public final TextView mNotes;
    public final TagView mTagGroup;


    public AnnouncementViewHolder(View view) {
        super(view);
        View outerView = getOuterView();
        View innerView = getInnerView();
        mTitle = outerView.findViewById(R.id.title);
        mSubTitle = outerView.findViewById(R.id.sub_title);
        mNotes = innerView.findViewById(R.id.notes);
        mTagGroup = innerView.findViewById(R.id.tag_group);

    }

    @NonNull
    @Override
    public String toString() {
        return super.toString() + " '" + mTitle.getText() + "' '" + mSubTitle.getText() + "'";
    }
}
