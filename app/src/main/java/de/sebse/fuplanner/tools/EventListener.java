package de.sebse.fuplanner.tools;

import java.util.HashMap;

public class EventListener<T> {
    private final HashMap<String, EventFunction<T>> list = new HashMap<>();

    public void add(String id,EventFunction<T> listener) {
        list.put(id, listener);
    }

    public void remove(String id) {
        list.remove(id);
    }

    public void emit(T value) {
        for (EventFunction<T> listener : list.values()) {
            listener.apply(value);
        }
    }

    @FunctionalInterface
    public interface EventFunction<T> {
        void apply(T value);
    }
}
